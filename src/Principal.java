import java.io.File;
import java.io.FileWriter;
import java.util.*;

public class Principal {

    private String nombre;

    /*
     *
     * @param maximo
     * @param intentos
     * @param jugadas
     */
    private void saveToFile( int maximo, int intentos, int jugadas) {
        try {
            String path = System.getProperty("user.dir") + "\\file.txt";
            File file = new File(path);
            System.out.println(path);
            FileWriter writer = new FileWriter(path, true);
            System.out.println("Done");
            writer.write("Jugada=" + jugadas + " Usuario=" + nombre + " Intentos=" +  intentos + " NumeroMaximo=" + maximo + "\n");
            writer.close();
        } catch (Exception ex) {
            this.sendMessage("no se ha podido guardar en el archivo");
        }
    }

    /*
     *
     * @param info
     */
    private void sendMessage(String info) {
        //
        if (nombre.equalsIgnoreCase("help") || nombre.equalsIgnoreCase("exit")) {
            System.out.println("JUEGO >> " + info);
        } else {
            System.out.println("JUEGO >> " + nombre + " " + info);
        }

    }

    /*
     *
     * @param nombre
     * @param sc
     * @param jugadas
     */
    private void jugar(String nombre, Scanner sc, int jugadas) {
        Random generador = new Random();
        while (true) {
            try {
                int maxNums = 0;
                this.sendMessage("indica un numero");
                // guardamos lo que el usuario nos ha introducido.
                String entry = sc.next();
                // comprobamos si lo entrado es exit o help, sino lo convertimos en int.
                if (!matchHelpOrExit(entry)) {
                    // convertimos la entrada en int, si no es un int saltará una excepción.
                    maxNums = Integer.parseInt(entry);
                }
                // comprobamos que el valor entrado sea >= 2.
                if (maxNums >= 2) {
                    int numeroGenerado = generador.nextInt(maxNums) + 1;
                    int intentos = 1;
                    while (true) {
                        try {
                            int numeroEntrado = 0;
                            this.sendMessage("entra un numero entre 1 y " + maxNums);
                            // guardamos lo que el usuario nos ha introducido.
                            entry = sc.next();
                            // comprobamos si lo entrado es exit o help, sino lo convertimos en int.
                            if (!matchHelpOrExit(entry)) {
                                // convertimos la entrada en int, si no es un int saltará una excepción.
                                numeroEntrado = Integer.parseInt(entry);
                                //
                                if ((numeroEntrado > 0) && (numeroEntrado <= maxNums)) {
                                    // igualamos dos valores
                                    if (numeroEntrado == numeroGenerado) {
                                        this.sendMessage("felicidades has ganado en " + intentos + " intentos");
                                        this.sendMessage("quieres volver a jugar?");
                                        while (true) {
                                            //
                                            String opcion = sc.next();
                                            //
                                            if (opcion.equalsIgnoreCase("SI") || opcion.equalsIgnoreCase("YES") || opcion.equalsIgnoreCase("Y")) {
                                                this.sendMessage("ahora ganaras a la primera");
                                                this.saveToFile(maxNums, intentos, jugadas);
                                                this.jugar(nombre, sc, ++jugadas);
                                            } else if (opcion.equalsIgnoreCase("NO") || opcion.equalsIgnoreCase("N")) {
                                                this.saveToFile(maxNums, intentos, jugadas);
                                                System.exit(0);
                                            } else {
                                                //
                                                if(!matchHelpOrExit(opcion)) {
                                                    this.sendMessage("Escribe SI o NO porfavor");
                                                } else {
                                                    this.sendMessage("quieres volver a jugar?");
                                                }
                                            }
                                        }
                                    } else {
                                        intentos++;
                                        //
                                        if (numeroEntrado < numeroGenerado) {
                                            this. sendMessage("el numero es mas grande del entrado");
                                        } else {
                                            this.sendMessage("el numero es mas pequeño del entrado");
                                        }
                                    }
                                } else {
                                    this.sendMessage("lo que has introducido es < a 1 y > a " + maxNums);
                                }
                            }
                        } catch (Exception ex) {
                            this.sendMessage("lo que has introducido no es un numero.");
                            sc.next();
                        }
                    }
                }
            } catch (Exception ex) {
                this.sendMessage("lo que has introducido no es un numero.");
            }
        }
    }

    public static void main(String[] args) {
        new Principal();
    }

    /*
     *
     * @param text
     * @return
     */
    private boolean matchHelpOrExit(String text) {
        if (text.equalsIgnoreCase("help")) {
            this.sendMessage("- Para poder jugar tienes que introducir primero un nombre" +
                    "\n- Despúes tienes que introducir un numero maximo" +
                    "\n- A continuación te pedirá que introduzcas números hasta que aciertes" +
                    "\n- Por ultimo te pedira si quieres volver a jugar");
            return true;
        } else if (text.equalsIgnoreCase("exit")) {
            this.sendMessage("Adios :(");
            System.exit(0);
        }
        return false;
    }

    public Principal() {
        Scanner sc = new Scanner(System.in);

        while (true) {
            System.out.println("¡Recuerda que puedes usar help en cualquier momento si no sabes como jugar!");
            System.out.println("Dime tu nombre porfavor:");
            this.nombre = sc.next();
            this.matchHelpOrExit(nombre);
            //
            if (nombre.length() > 0 && !(nombre.equalsIgnoreCase("help") || nombre.equalsIgnoreCase("exit"))) {
                break;
            }
        }
        this.sendMessage("¡Bienvenido!");
        this.jugar(nombre, sc, 1);
    }
}

